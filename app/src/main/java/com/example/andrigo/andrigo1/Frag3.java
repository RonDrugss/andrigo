package com.example.andrigo.andrigo1;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



public class Frag3 extends Fragment {

    EditText etUser, etPass;
    Button btF;

    public Frag3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_frag3,container,false);

        etUser = (EditText)view.findViewById(R.id.etUsername);
        etPass = (EditText)view.findViewById(R.id.etPassword);
        btF = (Button)view.findViewById(R.id.btFinal);

        btF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String user = etUser.getText().toString();
                String pass = etPass.getText().toString();

                if(!user.isEmpty() && !pass.isEmpty()){

                    Toast.makeText(getActivity(), "Su Contraseña es: " +pass, Toast.LENGTH_SHORT).show();


                }else{

                    Toast.makeText(getActivity(), "Los campos son obligatorios.", Toast.LENGTH_SHORT).show();

                }

            }
        });


        return view;
    }

}
