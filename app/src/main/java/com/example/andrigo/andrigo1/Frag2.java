package com.example.andrigo.andrigo1;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Frag2 extends Fragment {

    EditText etM, etF;
    Button btS;

    public Frag2() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_frag2,container,false);

        etM = (EditText)view.findViewById(R.id.etMail);
        etF = (EditText)view.findViewById(R.id.etPhone);
        btS = (Button) view.findViewById(R.id.btNextTwo);

        btS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = etM.getText().toString();
                String telefono = etF.getText().toString();

                if(!email.isEmpty()){
                    FragmentTransaction fr = getFragmentManager().beginTransaction();
                    fr.replace(R.id.fragment_container, new Frag3());
                    fr.commit();


                }else{
                    Toast.makeText(getActivity(), "El email es obligatorio", Toast.LENGTH_SHORT).show();
                }


            }
        });



        return view;
    }

}

